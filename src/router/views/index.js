import Layout from '@/page/index/';
export default [{
  path: '/dashboard',
  component: () => import('@/page/index/index.vue'),
  redirect: '/dashboard/index',
  children: [{
    path: 'index',
    name: '首页',
    meta: {
      i18n: 'dashboard'
    },
    component: () =>
      import('@/views/dashboard/index')
  }]
}, {
  path: '/info',
  component: Layout,
  redirect: '/info/index',
  children: [{
    path: 'index',
    name: '个人信息',
    meta: {
      i18n: 'info'
    },
    component: () =>
      import('@/views/user/info')
  }, {
    path: 'setting',
    name: '个人设置',
    meta: {
      i18n: 'setting'
    },
    component: () =>
      import('@/views/user/setting')
  }]
}];